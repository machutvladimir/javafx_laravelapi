module org.main.chatroom {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;
    requires java.net.http;
    requires org.json;
    requires com.google.gson;

    opens org.main.chatroom to javafx.fxml;
    exports org.main.chatroom;
}