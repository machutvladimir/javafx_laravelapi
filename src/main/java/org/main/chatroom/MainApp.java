package org.main.chatroom;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class MainApp extends Application {
    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(MainApp.class.getResource("Main.fxml")));
        Scene scene = new Scene(root);
        stage.setTitle("ChatRoom");
        stage.setScene(scene);
    /*
        stage.getIcons().add(new Image("images/icon.png"));
        stage.initStyle(StageStyle.UNDECORATED);
    */

        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();

    }
}