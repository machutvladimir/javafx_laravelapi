package org.main.chatroom;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.animation.FadeTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.main.chatroom.require.Connect2Backend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainController {

    public TextField total;
    public TextField min;
    public TextField max;
    @FXML
    private Button addBtn;
    @FXML
    private Button deleteBtn;
    @FXML
    private Button updateBtn;
    @FXML
    private TableView<MainModel> tableView;
    @FXML
    private TableColumn<MainModel, Integer> idColumn;
    @FXML
    private TableColumn<MainModel, String> numeroProduitColumn;
    @FXML
    private TableColumn<MainModel, String> designationColumn;
    @FXML
    private TableColumn<MainModel, Double> prixColumn;
    @FXML
    private TableColumn<MainModel, Integer> quantiteColumn;
    private MainModel modelCel;

    // Fetch data from the external API by Connect2Backend class
    @FXML
    protected void initialize() throws IOException {
        reloadTableView();
        updateBtn.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton modifier lorsque qu'une ligne est sélectionnée
        tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MainModel>() {
            @Override
            public void changed(ObservableValue<? extends MainModel> observable, MainModel oldValue, MainModel newValue) {
                updateBtn.setDisable(newValue == null);
                modelCel = newValue;
            }
        });

        deleteBtn.setDisable(true);
        // Ajout de l'écouteur pour activer le bouton supprimer lorsque qu'une ligne est sélectionnée
        tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<MainModel>() {
            @Override
            public void changed(ObservableValue<? extends MainModel> observable, MainModel oldValue, MainModel newValue) {
                deleteBtn.setDisable(newValue == null);
                modelCel = newValue;
            }
        });
    }
    private Integer prixmin ;
    private Integer prixmax = 0;
    private Integer totalval = 0;

    private void reloadTableView() throws IOException {
        Connect2Backend backendConnector = new Connect2Backend();
        String jsonResponse = String.valueOf(backendConnector.fetchData());
        List<MainModel> productList = new ArrayList<>();
        // Parse JSON response
        JsonArray jsonArray = JsonParser.parseString(jsonResponse).getAsJsonArray();

        // Set error value to null
        String err = null;

        // Iterate through each element in the array
        for (JsonElement element : jsonArray) {
            JsonObject jsonObject = element.getAsJsonObject();
            if (jsonObject.get("error") != null) {
                err = String.valueOf(jsonObject.get("error"));
                break;
            }
            // Get values for each key
            int id = jsonObject.get("id").getAsInt();
            String numProduit = jsonObject.get("num_produit").getAsString();
            String design = jsonObject.get("design").getAsString();
            int quantite = jsonObject.get("quantite").getAsInt();

            int prix = jsonObject.get("prix").getAsInt();
            if (prixmin != null)
                prixmin = Math.min(prixmin, prix);
            else prixmin=prix;
            prixmax = Math.max(prixmax, prix);
            totalval = totalval + (prix * quantite);

            // Create product
            MainModel p = new MainModel(id, numProduit, design, prix, quantite);

            // PopupController it
            productList.add(p);
        }
        if (err != null) {
            System.out.println("Empty value!!!");
            tableView.setItems(null);
        } else {
            // Convert the list to an ObservableList
            ObservableList<MainModel> observableList = FXCollections.observableArrayList(productList);
            // Populate the table view
            idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
            numeroProduitColumn.setCellValueFactory(new PropertyValueFactory<>("numeroProduit"));
            designationColumn.setCellValueFactory(new PropertyValueFactory<>("designation"));
            prixColumn.setCellValueFactory(new PropertyValueFactory<>("prix"));
            quantiteColumn.setCellValueFactory(new PropertyValueFactory<>("quantite"));

            tableView.setItems(observableList);
            min.setText(String.valueOf(prixmin));
            max.setText(String.valueOf(prixmax));
            total.setText(String.valueOf(totalval));
        }
    }

    @FXML
    protected void onClickAjouterButton() throws IOException {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Add.fxml"));
            Parent root = loader.load();
            PopupController prod = loader.getController();
            prod.requestParent(root);
            prod.request(tableView, idColumn, numeroProduitColumn, designationColumn, prixColumn, quantiteColumn);
            prod.setMVal(total,min,max);

            // Create a new stage for the popup window
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Add Product");
            stage.initModality(Modality.APPLICATION_MODAL);
            FadeTransition ft = new FadeTransition(Duration.millis(1000), root);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.play();
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void onClickSupprimerButton() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Delete.fxml"));
            Parent root = loader.load();

            PopupController prod = loader.getController();
            prod.requestParent(root);
            prod.request(tableView, idColumn, numeroProduitColumn, designationColumn, prixColumn, quantiteColumn);
            prod.setMVal(total,min,max);

            Text idHidden = (Text) root.lookup("#idHidden");
            idHidden.setText(String.valueOf(modelCel.getId()));

            TextField textField1 = (TextField) root.lookup("#num_prod");
            textField1.setText(modelCel.getNumeroProduit());

            TextField textField2 = (TextField) root.lookup("#design");
            textField2.setText(modelCel.getDesignation());

            TextField textField3 = (TextField) root.lookup("#prix");
            textField3.setText(String.valueOf(modelCel.getPrix()));

            TextField textField4 = (TextField) root.lookup("#quantite");
            textField4.setText(String.valueOf(modelCel.getQuantite()));

            // Create a new stage for the popup window
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Delete Product");
            stage.initModality(Modality.APPLICATION_MODAL);
            FadeTransition ft = new FadeTransition(Duration.millis(1000), root);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.play();
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void onClickModifierButton() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Update.fxml"));
            Parent root = loader.load();
            PopupController prod = loader.getController();
            prod.requestParent(root);
            prod.request(tableView, idColumn, numeroProduitColumn, designationColumn, prixColumn, quantiteColumn);
            prod.setMVal(total,min,max);

            Text idHidden = (Text) root.lookup("#idHidden");
            idHidden.setText(String.valueOf(modelCel.getId()));

            TextField textField1 = (TextField) root.lookup("#num_prod");
            textField1.setText(modelCel.getNumeroProduit());

            TextField textField2 = (TextField) root.lookup("#design");
            textField2.setText(modelCel.getDesignation());

            TextField textField3 = (TextField) root.lookup("#prix");
            textField3.setText(String.valueOf(modelCel.getPrix()));

            TextField textField4 = (TextField) root.lookup("#quantite");
            textField4.setText(String.valueOf(modelCel.getQuantite()));

            // Create a new stage for the popup window
            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.setTitle("Update Product");
            stage.initModality(Modality.APPLICATION_MODAL);
            FadeTransition ft = new FadeTransition(Duration.millis(1000), root);
            ft.setFromValue(0.0);
            ft.setToValue(1.0);
            ft.play();
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}