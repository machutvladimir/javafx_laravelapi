package org.main.chatroom;

public class MainModel {
    private String numeroProduit;
    private String designation;
    private int prix;
    private int quantite;
    private int id;

    public MainModel(int id, String numeroProduit, String designation, int prix, int quantite) {
        this.numeroProduit = numeroProduit;
        this.designation = designation;
        this.prix = prix;
        this.quantite = quantite;
        this.id = id;
    }


    public String getNumeroProduit() {
        return numeroProduit;
    }

    public String getDesignation() {
        return designation;
    }

    public int getPrix() {
        return prix;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public int getId() {
        return id;
    }
}
