package org.main.chatroom;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import javafx.animation.FadeTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.main.chatroom.require.Connect2Backend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PopupController {
    public Button addBtn;
    @FXML
    private TableView<MainModel> tableView;
    @FXML
    private TableColumn<MainModel, Integer> idColumn;
    @FXML
    private TableColumn<MainModel, String> numeroProduitColumn;
    @FXML
    private TableColumn<MainModel, String> designationColumn;
    @FXML
    private TableColumn<MainModel, Double> prixColumn;
    @FXML
    private TableColumn<MainModel, Integer> quantiteColumn;
    private Parent parent;

    public void requestParent(Parent root) {
        this.parent = root;
    }

    public void request(TableView<MainModel> tableView, TableColumn<MainModel, Integer> idColumn, TableColumn<MainModel, String> numeroProduitColumn, TableColumn<MainModel, String> designationColumn, TableColumn<MainModel, Double> prixColumn, TableColumn<MainModel, Integer> quantiteColumn) {
        this.tableView = tableView;
        this.idColumn = idColumn;
        this.numeroProduitColumn = numeroProduitColumn;
        this.designationColumn = designationColumn;
        this.prixColumn = prixColumn;
        this.quantiteColumn = quantiteColumn;
    }

    public void addClick(ActionEvent actionEvent) throws IOException {
        String num_prod = "";
        String design = "";
        int prix = 0;
        int quantite = 0;

        TextField textField1 = (TextField) parent.lookup("#num_prod");
        num_prod = textField1.getText();

        TextField textField2 = (TextField) parent.lookup("#design");
        design = textField2.getText();

        TextField textField3 = (TextField) parent.lookup("#prix");
        prix = Integer.parseInt(textField3.getText());

        TextField textField4 = (TextField) parent.lookup("#quantite");
        quantite = Integer.parseInt(textField4.getText());

//        System.out.println(num_prod +" "+ design +" "+ prix +" "+ quantite);
        Connect2Backend connect2Backend = new Connect2Backend();
        Boolean sucAjout = connect2Backend.sendData(num_prod, design, prix, quantite);

        if (sucAjout == Boolean.TRUE) {
            cancelClick(actionEvent);
            reloadTableView();
            alertSuccess();
        } else {
            alertError();
        }
    }

    public void updateClick(ActionEvent event) throws IOException {
        int id = 0;
        String num_prod = "";
        String design = "";
        int prix = 0;
        int quantite = 0;

        Text idText = (Text) parent.lookup("#idHidden");
        id = Integer.parseInt(idText.getText());

        TextField textField1 = (TextField) parent.lookup("#num_prod");
        num_prod = textField1.getText();

        TextField textField2 = (TextField) parent.lookup("#design");
        design = textField2.getText();

        TextField textField3 = (TextField) parent.lookup("#prix");
        prix = Integer.parseInt(textField3.getText());

        TextField textField4 = (TextField) parent.lookup("#quantite");
        quantite = Integer.parseInt(textField4.getText());

        System.out.println(id + " " + num_prod + " " + design + " " + prix + " " + quantite);
        Connect2Backend connect2Backend = new Connect2Backend();
        Boolean sucAjout = connect2Backend.updateData(id, num_prod, design, prix, quantite);

        if (sucAjout == Boolean.TRUE) {
            cancelClick(event);
            reloadTableView();
            alertSuccess();
        } else {
            alertError();
        }
    }

    public void deleteClick(ActionEvent event) throws IOException {
        int id = 0;
        String num_prod = "";
        String design = "";
        int prix = 0;
        int quantite = 0;

        Text idText = (Text) parent.lookup("#idHidden");
        id = Integer.parseInt(idText.getText());

        Connect2Backend connect2Backend = new Connect2Backend();
        Boolean sucAjout = connect2Backend.deleteData(id);

        if (sucAjout == Boolean.TRUE) {
            cancelClick(event);
            reloadTableView();
            alertSuccess();
        } else {
            alertError();
        }
    }

    public void cancelClick(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FadeTransition ft = new FadeTransition(Duration.millis(1000), stage.getScene().getRoot());
        ft.setFromValue(1.0);
        ft.setToValue(0.0);
        ft.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stage.close();
            }
        });
        ft.play();
    }

    //~~~~~~~~~~~~~~~~~~~~ RELOAD TABLE VIEW ~~~~~~~~~~~~~~~~~~~~~~
    private Integer prixmin ;
    private Integer prixmax = 0;
    private Integer totalval = 0;
    public TextField total;
    public TextField min;
    public TextField max;
    public void setMVal(TextField total, TextField min, TextField max){
        this.total = total;
        this.min = min;
        this.max = max;
    }
    private void reloadTableView() throws IOException {
        Connect2Backend backendConnector = new Connect2Backend();
        String jsonResponse = String.valueOf(backendConnector.fetchData());
        List<MainModel> productList = new ArrayList<>();
        // Parse JSON response
        JsonArray jsonArray = JsonParser.parseString(jsonResponse).getAsJsonArray();

        // Set error value to null
        String err = null;

        // Iterate through each element in the array
        for (JsonElement element : jsonArray) {
            JsonObject jsonObject = element.getAsJsonObject();
            if (jsonObject.get("error") != null) {
                err = String.valueOf(jsonObject.get("error"));
                break;
            }
            // Get values for each key
            int id = jsonObject.get("id").getAsInt();
            String numProduit = jsonObject.get("num_produit").getAsString();
            String design = jsonObject.get("design").getAsString();
            int prix = jsonObject.get("prix").getAsInt();
            int quantite = jsonObject.get("quantite").getAsInt();

            if (prixmin != null)
                prixmin = Math.min(prixmin, prix);
            else prixmin=prix;
            prixmax = Math.max(prixmax, prix);
            int pq = prix * quantite;
            totalval += pq;

            // Create product
            MainModel p = new MainModel(id, numProduit, design, prix, quantite);

            // PopupController it
            productList.add(p);
        }
        if (err != null) {
            System.out.println("Empty value!!!");
            tableView.setItems(null);
        } else {
            // Convert the list to an ObservableList
            ObservableList<MainModel> observableList = FXCollections.observableArrayList(productList);
            // Populate the table view
            numeroProduitColumn.setCellValueFactory(new PropertyValueFactory<>("numeroProduit"));
            designationColumn.setCellValueFactory(new PropertyValueFactory<>("designation"));
            prixColumn.setCellValueFactory(new PropertyValueFactory<>("prix"));
            quantiteColumn.setCellValueFactory(new PropertyValueFactory<>("quantite"));

            tableView.setItems(observableList);

            min.setText(String.valueOf(prixmin));
            max.setText(String.valueOf(prixmax));
            total.setText(String.valueOf(totalval));
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~ ALERT ~~~~~~~~~~~~~~~~~~~~~~
    void alertSuccess() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Succès");
        alert.setHeaderText(null);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setContentText("L'opération a été effectuée avec succès !");
        alert.showAndWait();
    }

    void alertError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erreur");
        alert.setHeaderText(null);
        alert.setContentText("Une erreur est survenu!\nVeuiller réessayer à nouveau s'il vous plaît!");
        alert.showAndWait();
    }

}
