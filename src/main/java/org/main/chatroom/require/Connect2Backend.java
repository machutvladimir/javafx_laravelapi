package org.main.chatroom.require;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class Connect2Backend {

    private final String hostAPIURL = "http://127.0.0.1:8000";

    public StringBuilder fetchData() throws IOException {
        try {
            URL url = new URL(hostAPIURL + "/api/products/");

            // Open connection
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Set request method
            connection.setRequestMethod("GET");

            // Get response code
            int responseCode = connection.getResponseCode();
            System.out.println("Response Code: " + responseCode);

            // Read response
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Print response
//        System.out.println("Response: " + response.toString());
            // Close connection
            connection.disconnect();
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Boolean sendData(String num_prod, String design, Integer prix, int quantite) {

        URL url = null;
        try {
            url = new URL(hostAPIURL + "/api/products/");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            connection.setRequestMethod("POST");
        } catch (ProtocolException e) {
            throw new RuntimeException(e);
        }
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        // Set the request body
        String requestBody = "{\"num_produit\": \"" + num_prod + "\", \"design\": \"" + design + "\",\"prix\": " + prix + ",\"quantite\": " + quantite + " }";
        OutputStream outputStream = null;
        try {
            outputStream = connection.getOutputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            outputStream.write(requestBody.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            outputStream.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        int responseCode = 0;
        try {
            responseCode = connection.getResponseCode();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        connection.disconnect();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            System.out.println("Successfully created");
            return Boolean.TRUE;
        } else {
            System.out.println("Handle error");
            return Boolean.FALSE;
        }
    }

    public Boolean updateData(int id, String num_prod, String design, Integer prix, int quantite) throws IOException {
        URL url = new URL(hostAPIURL + "/api/products/" + id);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("PUT"); // or "PATCH"
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        // Set the request body
        String requestBody = "{\"num_produit\": \"" + num_prod + "\", \"design\": \"" + design + "\",\"prix\": " + prix + ",\"quantite\": " + quantite + " }";
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(requestBody.getBytes());
        outputStream.flush();
        outputStream.close();

        connection.disconnect();
        int responseCode = connection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            System.out.println("Successfully created");
            return Boolean.TRUE;
        } else {
            System.out.println("Handle error");
            return Boolean.FALSE;
        }
    }

    public Boolean deleteData(int id) throws IOException {
        URL url = new URL(hostAPIURL + "/api/products/" + id);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("DELETE");

        int responseCode = connection.getResponseCode();
        connection.disconnect();
        if (responseCode == HttpURLConnection.HTTP_OK) {
            // Successfully deleted
            System.out.println("Successfully created");
            return Boolean.TRUE;
        } else {
            // Handle error
            System.out.println("Handle error");
            return Boolean.FALSE;
        }
    }
}
